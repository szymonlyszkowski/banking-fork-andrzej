package BANK.sharedkernel.enums;

/**
 * Created by szymonidas on 12/21/16.
 */
public enum DebitCardBands {
    AMERICAN_EXPRESS, VISA, MASTERCARD
}
