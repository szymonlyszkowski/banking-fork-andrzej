package BANK.domain.installment.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.installment.dto.InstallmentSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IInstallmentBO {

   InstallmentSnapshot add(Double amount,LocalDateTime finishDate,Long activeCreditId);

//   void delete(Long installmentId);

   void pay(Long installmentId);
}
