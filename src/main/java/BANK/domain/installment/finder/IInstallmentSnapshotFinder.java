package BANK.domain.installment.finder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import BANK.domain.installment.dto.InstallmentSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IInstallmentSnapshotFinder {

   InstallmentSnapshot findById(Long id);

   List<InstallmentSnapshot> findAll();

   List<InstallmentSnapshot> findAll(Set<Long> ids);

   List<InstallmentSnapshot> findActive();
   
   List<InstallmentSnapshot> findByActiveCreditId(Long creditId);
   
   List<InstallmentSnapshot> findByActiveTrueAndActiveCreditId(Long creditId);

}
