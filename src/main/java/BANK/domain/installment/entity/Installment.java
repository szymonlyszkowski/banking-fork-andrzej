package BANK.domain.installment.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Installment
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;
   
   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;
   
   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime finishDate;
   
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime paiedAt;

   @Version
   private long version;
   
   @NotNull
   private Double amount;
   
   private boolean active;
   
   private Long activeCreditId;
   

   protected Installment() {
   }

   public Installment( Double amount, LocalDateTime finishDate, Long activeCreditId) {
      this.createdAt = LocalDateTime.now();
      this.version = 1;
      this.amount = amount;
      this.active = true;
      this.finishDate = finishDate;
      this.activeCreditId = activeCreditId;
   }

   public void pay() {
      this.active = false;
      this.paiedAt = LocalDateTime.now();
   }

   public InstallmentSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }
      
      return new InstallmentSnapshot(id, createdAt, paiedAt, amount, version,active,finishDate,activeCreditId);
   }

   Long getId() {
      return id;
   }
}
