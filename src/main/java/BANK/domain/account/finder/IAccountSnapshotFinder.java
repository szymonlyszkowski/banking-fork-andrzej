package BANK.domain.account.finder;

import java.util.List;
import java.util.Set;

import BANK.domain.account.dto.AccountSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IAccountSnapshotFinder {

   AccountSnapshot findById(Long id);

   List<AccountSnapshot> findAll();
   
   List<AccountSnapshot> findByUserId(Long id);
   
   List<AccountSnapshot> findByActiveTrue();
   
   List<AccountSnapshot> findByActiveTrueAndUserId(Long id);

   AccountSnapshot findByIban(String iban);
}
