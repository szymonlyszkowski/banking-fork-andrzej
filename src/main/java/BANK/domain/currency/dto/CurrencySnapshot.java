package BANK.domain.currency.dto;

import java.time.LocalDateTime;

/**
 *
 * @author Mateusz.Glabicki
 */
public class CurrencySnapshot {

   private final Long id;

   private final String symbol;

   public CurrencySnapshot(Long id, String symbol) {
      this.id = id;
      this.symbol = symbol;
   }

   public Long getId() {
      return id;
   }

   public String getSymbol() {
      return symbol;
   }
   
}
