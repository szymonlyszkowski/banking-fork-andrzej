package BANK.domain.currency.bo;

import BANK.domain.currency.dto.CurrencySnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICurrencyBO {

   CurrencySnapshot add(String symbol);

   void delete(Long id);

   CurrencySnapshot edit(Long id, String symbol);
}
