package BANK.domain.user.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class UserSnapshot {

   private final Long id;

   private final UUID guid;

   private final String title;

   private final String firstname;

   private final String lastname;

   private final String username;

   private final LocalDateTime createdAt;

   private final String mail;

   private final boolean enabled;

   private final LocalDateTime blockedAt;

   private final LocalDateTime updatedAt;

   private final long version;
   
   private final String role;

   private final String password;
   
   public UserSnapshot(Long id, UUID guid, String title, String name, String lastname, String username,
      LocalDateTime createdAt,
      String mail,
      boolean enabled, LocalDateTime blockedAt,
      LocalDateTime updatedAt,
      long version,
      String role,String password) {
      this.id = id;
      this.guid = guid;
      this.title = title;
      this.firstname = name;
      this.lastname = lastname;
      this.username = username;
      this.createdAt = createdAt;
      this.mail = mail;
      this.enabled =  enabled;
      this.blockedAt = blockedAt;
      this.updatedAt = updatedAt;
      this.version = version;
      this.role =role;
      this.password = password;
   }

   public String getUsername() {
      return username;
   }

   public Long getId() {
      return id;
   }

   public UUID getGuid() {
      return guid;
   }

   public String getTitle() {
      return title;
   }

   public String getFirstname() {
      return firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public String getMail() {
      return mail;
   }

   public String getFullname() {
      return firstname + " " + lastname;
   }

   public boolean isEnabled() {
      return enabled;
   }


   public LocalDateTime getBlockedAt() {
      return blockedAt;
   }

  
   public LocalDateTime getUpdatedAt() {
      return updatedAt;
   }

   public long getVersion() {
      return version;
   }

   public String getRole() {
      return role;
   }

   public String getPassword() {
      return password;
   }
   
   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof UserSnapshot)) {
         return false;
      }
      UserSnapshot emp = (UserSnapshot) obj;
      return this.getId().equals(emp.getId());
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.id);
      return hash;
   }

}
