/**
 * This domain is concentrated on user management. It allows to access user data through REST API and manage
 * them in the database.
 */
package BANK.domain.user;
