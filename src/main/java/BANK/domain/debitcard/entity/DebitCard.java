package BANK.domain.debitcard.entity;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.debitcard.dto.DebitCardSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class DebitCard implements Serializable {

    private static final long serialVersionUID = 5512868628722882024L;

    @Id
    @GeneratedValue
    private Long cardId;

    @NotNull
    private String color;

    @NotNull
    //TODO join columns
    private Long brand;

    @NotNull
    private Long dailyLimitTransactionValue;

    @NotNull
    private Long dailyLimitTransactionNumber;

    @NotNull
    //TODO join columns
    private Long account;

    @NotNull
    //TODO join columns
    private Long technicalAccount;

    @NotNull
    private String cardStatus;

    @NotNull
    //TODO join columns
    private Long client;

    @NotNull
    private int pan;

    @NotNull
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime expiryDate;

    @NotNull
    private int cvv;

    @NotNull
    private int pin;

    @NotNull
    private boolean isSuspended;

    @NotNull
    private boolean isBlocked;

    public Boolean isSuspended() {
        return isSuspended;
    }

    public void suspend() {
        isSuspended = Boolean.TRUE;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }

    public void block() {
        isBlocked = Boolean.TRUE;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public DebitCard(String color, Long brand, Long dailyLimitTransactionValue, Long dailyLimitTransactionNumber,
                     Long account, Long technicalAccount, String cardStatus, Long client, int pan,
                     LocalDateTime expiryDate, int cvv, int pin) {
        this.color = color;
        this.brand = brand;
        this.dailyLimitTransactionValue = dailyLimitTransactionValue;
        this.dailyLimitTransactionNumber = dailyLimitTransactionNumber;
        this.account = account;
        this.technicalAccount = technicalAccount;
        this.cardStatus = cardStatus;
        this.client = client;
        this.pan = pan;
        this.expiryDate = expiryDate;
        this.cvv = cvv;
        this.pin = pin;
        this.isBlocked = Boolean.FALSE;
        this.isSuspended = Boolean.FALSE;
    }

    public DebitCard() {
    }

    public DebitCardSnapshot toSnapshot() {
        if (cardId == null) {
            throw new EntityInStateNewException();
        }

        return new DebitCardSnapshot(cardId, color, brand, dailyLimitTransactionValue, dailyLimitTransactionNumber, account,
                technicalAccount, cardStatus, client, pan, expiryDate, cvv, pin);
    }

    public void setDailyLimitTansactionValue(Long dailyLimitTransactionValue) {
        this.dailyLimitTransactionValue = dailyLimitTransactionValue;
    }

    public void setDailiyLimitTransactionNumber(Long dailyLimitTransactionNumber) {
        this.dailyLimitTransactionNumber = dailyLimitTransactionNumber;
    }
}
