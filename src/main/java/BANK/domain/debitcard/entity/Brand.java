package BANK.domain.debitcard.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Brand implements Serializable {

    private static final long serialVersionUID = 6088580414522799229L;

    @Id
    @GeneratedValue
    @NotNull
    private Long id;

    @NotNull
    private String brandName;

    @NotNull
    private String nationality;

    public Brand() {
    }

    public Brand(String brandName, String nationality) {
        this.brandName = brandName;
        this.nationality = nationality;
    }

}
