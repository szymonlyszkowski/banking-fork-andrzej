package BANK.domain.debitcard.dto;

import java.time.LocalDateTime;

public class TechnicalAccountSnapshot {

    private final Long technicalAccountId;

    private final LocalDateTime createdAt;

    private final Long balance;

    private final Long transaction;

    public TechnicalAccountSnapshot(Long technicalAccountId, LocalDateTime createdAt, Long balance, Long transaction) {
        this.technicalAccountId = technicalAccountId;
        this.createdAt = createdAt;
        this.balance = balance;
        this.transaction = transaction;
    }

    public Long getTechnicalAccountId() {
        return technicalAccountId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Long getBalance() {
        return balance;
    }

    public Long getTransaction() {
        return transaction;
    }
}
