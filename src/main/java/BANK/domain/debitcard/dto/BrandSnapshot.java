package BANK.domain.debitcard.dto;

public class BrandSnapshot {

    private final Long id;

    private final String brandName;

    private final String nationality;

    public BrandSnapshot(Long id, String brandName, String nationality) {
        this.id = id;
        this.brandName = brandName;
        this.nationality = nationality;
    }

    public Long getId() {
        return id;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getNationality() {
        return nationality;
    }
}
