package BANK.domain.debitcard.finder;

import BANK.domain.debitcard.dto.DebitCardSnapshot;
import BANK.domain.debitcard.entity.DebitCard;
import BANK.domain.debitcard.repo.IDebitCardRepository;
import BANK.sharedkernel.annotations.Finder;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lyszkows on 13/01/2017.
 */
@Finder
public class DebitCardSnapshotFinder implements IDebitCardSnapshotFinder {

    private final IDebitCardRepository debitCardRepository;

    @Autowired
    public DebitCardSnapshotFinder(IDebitCardRepository debitCardRepository) {
        this.debitCardRepository = debitCardRepository;
    }

    @Override
    public List<DebitCardSnapshot> findAll() {
        return convert(debitCardRepository.findAll());
    }

    @Override
    public DebitCardSnapshot findById(Long id) {
        DebitCard debitCard = debitCardRepository.findOne(id);
        return debitCard == null ? null : debitCard.toSnapshot();
    }

    private List<DebitCardSnapshot> convert(List<DebitCard> debitCards){
        return debitCards.stream().map(DebitCard::toSnapshot).collect(Collectors.toList());
    }
}
