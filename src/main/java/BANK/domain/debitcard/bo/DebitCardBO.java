package BANK.domain.debitcard.bo;

import BANK.domain.debitcard.dto.DebitCardSnapshot;
import BANK.domain.debitcard.entity.DebitCard;
import BANK.domain.debitcard.repo.IDebitCardRepository;
import BANK.sharedkernel.annotations.BussinesObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

@BussinesObject
public class DebitCardBO implements IDebitCardBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebitCardBO.class);
    private final IDebitCardRepository debitCardRepository;

    @Autowired
    public DebitCardBO(IDebitCardRepository debitCardRepository) {
        this.debitCardRepository = debitCardRepository;
    }

    @Override
    public DebitCardSnapshot add(String color, Long brand, Long dailyLimitTransactionValue,
                                 Long dailyLimitTransactionNumber, Long account, Long technicalAccount,
                                 String cardStatus, Long client, int pan, LocalDateTime expiryDate, int cvv, int pin) {
        DebitCard debitCard = new DebitCard(color, brand, dailyLimitTransactionValue, dailyLimitTransactionNumber, account,
                technicalAccount, cardStatus, client, pan, expiryDate, cvv, pin);

        debitCard = debitCardRepository.save(debitCard);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        LOGGER.info("Add debit card <{}>", debitCardSnapshot.getCardId());
        return debitCardSnapshot;
    }

    @Override
    public void delete(Long debitCardId) {
        final DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        debitCardRepository.delete(debitCard);
        LOGGER.info("Deleted debit card <{}>", debitCardId);
    }


    @Override
    public DebitCardSnapshot setPin(Long id, Integer newPin) {
        DebitCard debitCard = debitCardRepository.findOne(id);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setPin(newPin);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> PIN number set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getPin());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot setDailyLimitValue(Long debitCardId, Long dailyLimitTransactionValue) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setDailyLimitTansactionValue(dailyLimitTransactionValue);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> daily transaction value set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getDailyLimitTransactionValue());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot setDailyLimitTransactionNumber(Long debitCardId, Long dailyLimitTransactionNumber) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setDailiyLimitTransactionNumber(dailyLimitTransactionNumber);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> daily transaction number set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getDailyLimitTransactionNumber());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot suspendDebitCard(Long debitCardId) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.suspend();
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> suspended",
                debitCardSnapshot.getCardId(), debitCardSnapshot.isSuspended());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot blockDebitCard(Long debitCardId) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.block();
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> blocked",
                debitCardSnapshot.getCardId(), debitCardSnapshot.isBlocked());
        return debitCardSnapshot;
    }

}
