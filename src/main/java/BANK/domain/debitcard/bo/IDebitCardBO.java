package BANK.domain.debitcard.bo;

import BANK.domain.debitcard.dto.DebitCardSnapshot;
import java.time.LocalDateTime;

public interface IDebitCardBO {

    DebitCardSnapshot add(String color, Long brand, Long dailyLimitTransactionValue, Long dailyLimitTransactionNumber,
                          Long account, Long technicalAccount, String cardStatus, Long client, int pan,
                          LocalDateTime expiryDate, int cvv, int pin);

    void delete(Long debitCardId);

    DebitCardSnapshot setPin(Long id, Integer newPin);

    DebitCardSnapshot setDailyLimitValue(Long debitCardId, Long dailyLimitTransactionValue);

    DebitCardSnapshot setDailyLimitTransactionNumber(Long debitCardId, Long dailyLimitTransactionValue);

    DebitCardSnapshot suspendDebitCard(Long debitCardId);

    DebitCardSnapshot blockDebitCard(Long debitCardId);
}
