package BANK.domain.debitcard.repo;

import BANK.domain.debitcard.entity.DebitCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDebitCardRepository extends JpaRepository<DebitCard, Long> {

}
