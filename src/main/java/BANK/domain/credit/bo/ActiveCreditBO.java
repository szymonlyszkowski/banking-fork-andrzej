package BANK.domain.credit.bo;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.entity.ActiveCredit;
import BANK.domain.credit.entity.Credit;
import BANK.domain.credit.finder.IActiveCreditSnapshotFinder;
import BANK.domain.credit.finder.ICreditSnapshotFinder;
import BANK.domain.credit.repo.IActiveCreditRepository;
import BANK.domain.credit.repo.ICreditRepository;
import BANK.domain.installment.bo.IInstallmentBO;
import BANK.domain.installment.bo.InstallmentBO;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class ActiveCreditBO
   implements IActiveCreditBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(ActiveCreditBO.class);
   private final IActiveCreditRepository creditRepository;
   private final IInstallmentBO installmentBO;
   private final ICreditSnapshotFinder creditSnapshotFinder;

   
   @Autowired
   public ActiveCreditBO(IActiveCreditRepository creditRepository, IInstallmentBO installmentBO,ICreditSnapshotFinder creditSnapshotFinder) {
      this.creditRepository = creditRepository;
      this.installmentBO = installmentBO;
      this.creditSnapshotFinder = creditSnapshotFinder;
   }

   @Override
   public ActiveCreditSnapshot add(Long creditId, Long userId, LocalDateTime finishDate, Double value) {

      ActiveCredit credit = new ActiveCredit(creditId,userId,finishDate,value);

      credit = creditRepository.save(credit);
      
      ActiveCreditSnapshot activeCreditSnapshot = credit.toSnapshot();
      
      LOGGER.info("Add Credit <{}>", activeCreditSnapshot.getId());
      
      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(creditId);
      Double toPay  = activeCreditSnapshot.getValue()*creditSnapshot.getProvision()/100 + activeCreditSnapshot.getValue()*creditSnapshot.getInterest()/100 + activeCreditSnapshot.getValue();
      if(creditSnapshot.isInsurance()){
         toPay += activeCreditSnapshot.getValue()*0.02;
      }
      Long timeOfCredit = ChronoUnit.MONTHS.between(activeCreditSnapshot.getStartDate(), activeCreditSnapshot.getFinishDate());
      Double countOfRate = Math.floor(timeOfCredit/creditSnapshot.getInstallment());
      Long rate = countOfRate.longValue() + 1;
      
      LocalDateTime nextRate = activeCreditSnapshot.getStartDate();
      Double perRate = new Double(Math.round((toPay/rate)*100)/100);
      if(creditSnapshot.isChangingRate()){
         Double diff ;
         if(perRate > 10000){
            diff= 0.10;
         }else{
            diff = 0.05;
         }
         for(int i = 0; i < rate ; i++){
            perRate = new Double((Math.round((toPay/(rate-i))*100)/100) );
            if( i < rate - 1 ){
               nextRate = nextRate.plusMonths(creditSnapshot.getInstallment());
               perRate += perRate*diff*(rate-i);
               toPay-= perRate;
            }else{
               nextRate = activeCreditSnapshot.getFinishDate();
               perRate = toPay;
            }
            
            installmentBO.add(perRate, nextRate, activeCreditSnapshot.getId());
         }
      }else{
         perRate = new Double(Math.round((toPay/rate)*100)/100);
         for(int i = 0; i < rate ; i++){
            if( i < rate - 1 ){
               nextRate = nextRate.plusMonths(creditSnapshot.getInstallment());
            }else{
               nextRate = activeCreditSnapshot.getFinishDate();
            }
            installmentBO.add(perRate, nextRate, activeCreditSnapshot.getId());
         }
      }


      return activeCreditSnapshot;
   }
   
   @Override
   public void finish(Long activeCreditId) {
      ActiveCredit credit = creditRepository.findOne(activeCreditId);
      credit.finish();
      creditRepository.save(credit);

      ActiveCreditSnapshot creditSnapshot = credit.toSnapshot();
      
      LOGGER.info("Credit <{}>  marked as finished",   activeCreditId);
   }
   
   //   @Override
//   public CreditSnapshot edit(Long id, LocalDateTime startDate, LocalDateTime finishDate,
//      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance) {
//
//      Credit credit = creditRepository.findOne(id);
//
//      credit.editCredit(startDate,finishDate,provision,installment,rateOfInterest, interest,insurance);
//
//      creditRepository.save(credit);
//
//      CreditSnapshot creditSnapshot = credit.toSnapshot();
//
//      LOGGER.info("Edit Credit <{}>", creditSnapshot.getId());
//      return creditSnapshot;
//   }

//   @Override
//   public void delete(Long creditId) {
//      Credit credit = creditRepository.findOne(creditId);
//      creditRepository.delete(credit);
//
//      LOGGER.info("Credit <{}> marked as deleted", creditId);
//   }
}
