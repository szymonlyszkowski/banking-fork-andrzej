package BANK.domain.credit.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class ActiveCreditSnapshot {


   private final Long id;

   private final LocalDateTime startDate;
   
   private final LocalDateTime finishDate;

   private final boolean active;
   
   private final Long creditId;
   
   private final Long userId;
   
   private final Double value;
   
   public ActiveCreditSnapshot(Long id, LocalDateTime startDate, LocalDateTime finishDate,  boolean active,  Long creditId, Long userId, Double value) {
      this.id = id;
      this.startDate = startDate;
      this.finishDate = finishDate;
      this.active = active;
      this.creditId = creditId;
      this.userId = userId;
      this.value = value;
   }

   public Long getUserId() {
      return userId;
   }

   
   public Long getCreditId() {
      return creditId;
   }

   public Long getId() {
      return id;
   }

   public LocalDateTime getStartDate() {
      return startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public boolean isActive() {
      return active;
   }

   public Double getValue() {
      return value;
   }

   
   
   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof ActiveCreditSnapshot)) {
         return false;
      }
      ActiveCreditSnapshot emp = (ActiveCreditSnapshot) obj;
      return this.getId().equals(emp.getId());
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.id);
      return hash;
   }

}
