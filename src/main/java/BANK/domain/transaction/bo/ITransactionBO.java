package BANK.domain.transaction.bo;

import BANK.domain.transaction.dto.TransactionSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ITransactionBO {

   TransactionSnapshot add(String fromIBAN, String toIBAN, Double value, Long currencyId);

   void delete(Long id);

}
