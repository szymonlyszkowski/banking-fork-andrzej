package BANK.domain.transaction.finder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import BANK.domain.transaction.dto.TransactionSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ITransactionSnapshotFinder {

   TransactionSnapshot findById(Long id);

   List<TransactionSnapshot> findAll();

//   TransactionSnapshot findByFromIBAN(String fromIBAN);
//   
//   TransactionSnapshot findByToIBAN(String toIBAN);
   
   List<TransactionSnapshot> findByCreatedAtBetween(LocalDateTime before, LocalDateTime after);
}
