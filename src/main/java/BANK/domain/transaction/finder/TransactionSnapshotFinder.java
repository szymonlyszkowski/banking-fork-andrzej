package BANK.domain.transaction.finder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import BANK.domain.transaction.dto.TransactionSnapshot;
import BANK.domain.transaction.entity.Transaction;
import BANK.domain.transaction.repo.ITransactionRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class TransactionSnapshotFinder
   implements ITransactionSnapshotFinder {

   private final ITransactionRepository transactionRepository;

   @Autowired
   public TransactionSnapshotFinder(ITransactionRepository transactionRepository) {
      this.transactionRepository = transactionRepository;
   }

   private List<TransactionSnapshot> convert(List<Transaction> transactions) {
      return transactions.stream()
         .map(Transaction::toTransactionSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public TransactionSnapshot findById(Long id) {
      Transaction transaction = transactionRepository.findOne(id);
      return transaction == null ? null : transaction.toTransactionSnapshot();
   }

//
//   @Override
//   public TransactionSnapshot findByFromIBAN(String fromIBAN) {
//      List<Transaction> transactions = transactionRepository.findByFromIBAN(fromIBAN);
//
//      return transactions.isEmpty() ? null : transactions.get(0).toTransactionSnapshot();
//   }
//   
//   @Override
//   public TransactionSnapshot findByToIBAN(String toIBAN) {
//      List<Transaction> transactions = transactionRepository.findByToIBAN(toIBAN);
//
//      return transactions.isEmpty() ? null : transactions.get(0).toTransactionSnapshot();
//   }

   @Override
   public List<TransactionSnapshot> findByCreatedAtBetween(LocalDateTime before, LocalDateTime after) {
      List<Transaction> transactions = transactionRepository.findByCreatedAtBetween(before,after);

      return convert(transactions);
   }
   
   @Override
   public List<TransactionSnapshot> findAll() {
      List<Transaction> transactions = transactionRepository.findAll();

      return convert(transactions);
   }

}
