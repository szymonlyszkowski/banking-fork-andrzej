package BANK.web.restapi.debitcard;

import javax.validation.constraints.NotNull;

/**
 * Created by lyszkows on 14/01/2017.
 */
public class DebitCardEdit {
    @NotNull
    private Long debitCardId;

    @NotNull
    private Integer pin;

    @NotNull
    private Long dailyLimitTransactionValue;

    @NotNull
    private  Long dailyLimitTransactionNumber;

    @NotNull
    private Boolean isSuspended;

    @NotNull
    private Boolean isBlocked;

    public Boolean isSuspended() {
        return isSuspended;
    }

    public void suspend() {
        isSuspended = Boolean.TRUE;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }

    public void block() {
        isBlocked = Boolean.TRUE;
    }

    public Long getDebitCardId() {
        return debitCardId;
    }

    public void setDebitCardId(Long debitCardId) {
        this.debitCardId = debitCardId;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public Long getDailyLimitTransactionValue() {
        return dailyLimitTransactionValue;
    }

    public void setDailyLimitTransactionValue(Long dailyLimitTransactionValue) {
        this.dailyLimitTransactionValue = dailyLimitTransactionValue;
    }

    public Long getDailyLimitTransactionNumber() {
        return dailyLimitTransactionNumber;
    }

    public void setDailyLimitTransactionNumber(Long dailyLimitTransactionNumber) {
        this.dailyLimitTransactionNumber = dailyLimitTransactionNumber;
    }
}
