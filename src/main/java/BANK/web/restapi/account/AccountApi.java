package BANK.web.restapi.account;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.finder.IAccountSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.account.bo.IAccountBO;
import BANK.domain.transaction.bo.ITransactionBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

@RestController
@RequestMapping("/api/account")
public class AccountApi {

   private final IAccountSnapshotFinder accountSnapshotFinder;

   private final IUserSnapshotFinder userSnapshotFinder;

   private final IAccountBO accountBO;

   private final ITransactionBO transactionBO;

   @Autowired
   public AccountApi(ITransactionBO transactionBO, IAccountSnapshotFinder accountSnapshotFinder, IAccountBO accountBO,
      IUserSnapshotFinder userSnapshotFinder) {
      this.accountSnapshotFinder = accountSnapshotFinder;
      this.accountBO = accountBO;
      this.userSnapshotFinder = userSnapshotFinder;
      this.transactionBO = transactionBO;
   }

   @RequestMapping(method = RequestMethod.GET)
   public List<Account> myAccountList(Principal principal) {

      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());

      List<AccountSnapshot> accountSnapshots = accountSnapshotFinder.findByUserId(userSnapshot.getId());

      return accountSnapshots.stream()
         .map(Account::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/user/{userId}",
      method = RequestMethod.GET)
   public List<Account> userList(@PathVariable("userId") long userId) {
      List<AccountSnapshot> accountSnapshots = accountSnapshotFinder.findByUserId(userId);

      return accountSnapshots.stream()
         .map(Account::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<Account> active() {
      List<AccountSnapshot> accountSnapshots = accountSnapshotFinder.findByActiveTrue();

      return accountSnapshots.stream()
         .map(Account::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/{id}",
      method = RequestMethod.GET)
   public HttpEntity<Account> get(@PathVariable("id") long id) {
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findById(id);

      if (accountSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);
      }
   }

   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Account> add(@RequestBody AccountNew accountNew, Principal principal) {

//      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      UserSnapshot userSnapshot = userSnapshotFinder.findById(accountNew.getUserId());
      AccountSnapshot accountSnapshot;
      if (accountSnapshotFinder.findAll().isEmpty()) {
         accountSnapshot = accountBO.add(accountNew.getIban(), accountNew.getBalance(), accountNew.getCurrencyId(), true,
            userSnapshot.getId());
      } else {
         accountSnapshot = accountBO.add(accountNew.getIban(), accountNew.getBalance(), accountNew.getCurrencyId(),
            false, userSnapshot.getId());
      }

      transactionBO.add("1", accountSnapshot.getIban(), accountNew.getBalance(), accountSnapshot.getCurrencyId());

      return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);
   }

   @RequestMapping(value = "/payment",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Account> payment(@RequestBody Payment payment) {

      AccountSnapshot accountSnapshot = accountBO.boost(payment.getAccountId(), payment.getBalance());

      transactionBO.add("1", accountSnapshot.getIban(), payment.getBalance(), accountSnapshot.getCurrencyId());

      return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);

   }

//   @RequestMapping(method = RequestMethod.PUT,
//      consumes = MediaType.APPLICATION_JSON_VALUE)
//   public HttpEntity<Account> update(
//      @RequestBody AccountEdit accountEdit) {
//
//      AccountSnapshot foundedAccount = accountSnapshotFinder.findById(accountEdit.getAccountId());
//
//      AccountSnapshot accountSnapshot = accountBO.edit(accountEdit.getAccountId(),accountEdit.getIban);
//
//      return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);
//   }
   @RequestMapping(value = "active/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<Account> active(@PathVariable("id") Long accountId) {
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findById(accountId);

      if (accountId != null) {
         accountBO.active(accountId);
         return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.NOT_FOUND);
   }

   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long accountId) {
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findById(
         accountId);

      if (accountId != null) {
         accountBO.delete(accountId);
      }

      return new ResponseEntity<>(new Account(accountSnapshot), HttpStatus.OK);
   }
}
