package BANK.web.restapi.activeCredit;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.credit.dto.ActiveCreditSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */
public class ActiveCredit
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long activeCreditId;

   private LocalDateTime startDate;

   private LocalDateTime finishDate;

   private boolean active;

   private Long userId;

   private Long creditId;

   private Double value;

   private Double toPay;

   public ActiveCredit(ActiveCreditSnapshot activeCreditSnapshot) {
      this.activeCreditId = activeCreditSnapshot.getId();
      this.finishDate = activeCreditSnapshot.getFinishDate();
      this.startDate = activeCreditSnapshot.getStartDate();
      this.active = activeCreditSnapshot.isActive();
      this.userId = activeCreditSnapshot.getUserId();
      this.creditId = activeCreditSnapshot.getCreditId();
      this.value = activeCreditSnapshot.getValue();
      this.toPay = 0.0;
   }

   public ActiveCredit(ActiveCreditSnapshot activeCreditSnapshot,Double toPay) {
      this.activeCreditId = activeCreditSnapshot.getId();
      this.finishDate = activeCreditSnapshot.getFinishDate();
      this.startDate = activeCreditSnapshot.getStartDate();
      this.active = activeCreditSnapshot.isActive();
      this.userId = activeCreditSnapshot.getUserId();
      this.creditId = activeCreditSnapshot.getCreditId();
      this.value = activeCreditSnapshot.getValue();
      this.toPay = toPay;
   }

   public Long getActiveCreditId() {
      return activeCreditId;
   }

   public LocalDateTime getStartDate() {
      return startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public boolean isActive() {
      return active;
   }

   public Long getUserId() {
      return userId;
   }

   public Long getCreditId() {
      return creditId;
   }

   public Double getValue() {
      return value;
   }

   public Double getToPay() {
      return toPay;
   }
   
   
}
