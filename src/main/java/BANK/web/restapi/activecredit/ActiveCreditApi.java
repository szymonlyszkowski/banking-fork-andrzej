package BANK.web.restapi.activeCredit;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.finder.IActiveCreditSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.account.bo.IAccountBO;
import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.finder.IAccountSnapshotFinder;
import BANK.domain.credit.bo.IActiveCreditBO;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.finder.ICreditSnapshotFinder;
import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.domain.installment.finder.IInstallmentSnapshotFinder;
import BANK.domain.transaction.bo.ITransactionBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

@RestController
@RequestMapping("/api/activeCredit")
public class ActiveCreditApi {

   private final IActiveCreditSnapshotFinder activeCreditSnapshotFinder;

   private final IUserSnapshotFinder userSnapshotFinder;

   private final IActiveCreditBO activeCreditBO;

   private final ICreditSnapshotFinder creditSnapshotFinder;

   private final IAccountSnapshotFinder accountSnapshotFinder;

   private final IInstallmentSnapshotFinder iInstallmentSnapshotFinder;

   private final ITransactionBO transactionBO;

   private final IAccountBO accountBO;

   @Autowired
   public ActiveCreditApi(IActiveCreditSnapshotFinder activeCreditSnapshotFinder, IActiveCreditBO activeCreditBO,
      IUserSnapshotFinder userSnapshotFinder, IInstallmentSnapshotFinder iInstallmentSnapshotFinder,
      IAccountSnapshotFinder accountSnapshotFinder, ITransactionBO transactionBO,
      ICreditSnapshotFinder creditSnapshotFinder, IAccountBO accountBO) {
      this.activeCreditSnapshotFinder = activeCreditSnapshotFinder;
      this.activeCreditBO = activeCreditBO;
      this.userSnapshotFinder = userSnapshotFinder;
      this.transactionBO = transactionBO;
      this.accountSnapshotFinder = accountSnapshotFinder;
      this.creditSnapshotFinder = creditSnapshotFinder;
      this.accountBO = accountBO;
      this.iInstallmentSnapshotFinder = iInstallmentSnapshotFinder;
   }

   @RequestMapping(method = RequestMethod.GET)
   public List<ActiveCredit> myActiveCredit(Principal principal) {

      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());

      List<ActiveCreditSnapshot> activeCreditSnapshots = activeCreditSnapshotFinder.findByUserId(userSnapshot.getId());

      List<ActiveCredit> activeCredits = new ArrayList<>();
      for (ActiveCreditSnapshot acs : activeCreditSnapshots) {
         List<InstallmentSnapshot> installmentSnapshots = iInstallmentSnapshotFinder.findByActiveTrueAndActiveCreditId(
            acs.getId());
         Double toPay = 0.0;

         for (InstallmentSnapshot is : installmentSnapshots) {
            toPay += is.getAmount();
         }

         activeCredits.add(new ActiveCredit(acs, toPay));
      }

      return activeCredits;
   }

   @RequestMapping(value = "/user/{userId}",
      method = RequestMethod.GET)
   public List<ActiveCredit> userList(@PathVariable("userId") long userId) {
      List<ActiveCreditSnapshot> activeCreditSnapshots = activeCreditSnapshotFinder.findByUserId(userId);

      return activeCreditSnapshots.stream()
         .map(ActiveCredit::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<ActiveCredit> active() {
      List<ActiveCreditSnapshot> activeCreditSnapshots = activeCreditSnapshotFinder.findActive();

      return activeCreditSnapshots.stream()
         .map(ActiveCredit::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/{id}",
      method = RequestMethod.GET)
   public HttpEntity<ActiveCredit> get(@PathVariable("id") long id) {
      ActiveCreditSnapshot activeCreditSnapshot = activeCreditSnapshotFinder.findById(id);

      if (activeCreditSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.OK);
      }
   }

   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<ActiveCredit> add(@RequestBody ActiveCreditNew activeCreditNew, Principal principal) {

//      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      UserSnapshot userSnapshot = userSnapshotFinder.findById(activeCreditNew.getUserId());

      ActiveCreditSnapshot activeCreditSnapshot = activeCreditBO.add(activeCreditNew.getCreditId(),
         userSnapshot.getId(), activeCreditNew.getFinishDate(), activeCreditNew.getValue());

      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrue().get(0);

      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(activeCreditNew.getCreditId());

      transactionBO.add("1", accountSnapshot.getIban(), activeCreditSnapshot.getValue(), accountSnapshot.getCurrencyId());

      accountBO.boost(accountSnapshot.getId(), activeCreditSnapshot.getValue());

      return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.OK);
   }

//   @RequestMapping(method = RequestMethod.PUT,
//      consumes = MediaType.APPLICATION_JSON_VALUE)
//   public HttpEntity<ActiveCredit> update(
//      @RequestBody ActiveCreditEdit activeCreditEdit) {
//
//      ActiveCreditSnapshot foundedActiveCredit = activeCreditSnapshotFinder.findById(activeCreditEdit.getActiveCreditId());
//
//      ActiveCreditSnapshot activeCreditSnapshot = activeCreditBO.edit(activeCreditEdit.getActiveCreditId(),activeCreditEdit.getProvision(),activeCreditEdit.getInterest(),activeCreditEdit.getInstallment(),
//         activeCreditEdit.getContribution(),activeCreditEdit.isChangingRate(),activeCreditEdit.isInsurance());
//
//      return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.OK);
//   }
   @RequestMapping(value = "finish/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<ActiveCredit> finish(@PathVariable("id") Long activeCreditId) {
      ActiveCreditSnapshot activeCreditSnapshot = activeCreditSnapshotFinder.findById(activeCreditId);

      if (activeCreditId != null) {
         activeCreditBO.finish(activeCreditId);
         return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.NOT_FOUND);
   }

//   @RequestMapping(value = "delete/{id}",
//      method = RequestMethod.DELETE)
//   public HttpEntity delete(@PathVariable("id") Long activeCreditId) {
//      ActiveCreditSnapshot activeCreditSnapshot = activeCreditSnapshotFinder.findById(
//         activeCreditId);
//      
//      if (activeCreditId != null) {
//         activeCreditBO.delete(activeCreditId);
//      }
//
//      return new ResponseEntity<>(new ActiveCredit(activeCreditSnapshot), HttpStatus.OK);
//   }
}
