/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.activeCredit;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.web.restapi.annotations.RangeOfTimestamp;


/**
 *
 * @author Mateusz
 */

public class ActiveCreditNew {
   
   @NotNull
   private LocalDateTime finishDate;

   
   @NotNull
   private Long creditId;
   
   @NotNull
   private Long userId;
   
   @NotNull
   private Double value;

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public void setFinishDate(LocalDateTime finishDate) {
      this.finishDate = finishDate;
   }

   public Long getCreditId() {
      return creditId;
   }

   public void setCreditId(Long creditId) {
      this.creditId = creditId;
   }

   public Double getValue() {
      return value;
   }

   public void setValue(Double value) {
      this.value = value;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }
}
