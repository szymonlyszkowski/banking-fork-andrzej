/**
 * This package delivers REST API to log client side UI errors on server side. It could be modified to store logs in the
 * database or other data store.
 */
package BANK.web.restapi.logger;
