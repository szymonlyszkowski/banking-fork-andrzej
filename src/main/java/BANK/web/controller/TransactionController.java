package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TransactionController {

  @RequestMapping("/transaction/list")
   public String list() {
      return "transaction/list";
   }

}
