package BANK.config.development;

import java.time.LocalDateTime;
import java.time.Month;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.credit.bo.IActiveCreditBO;
import BANK.sharedkernel.constant.Profiles;


@Component
@Profile(Profiles.DEVELOPMENT)
public class ActiveCreditInitializer implements IActiveCreditInitializer{

   @Autowired
   private IActiveCreditBO activeCreditBO;
   
   @Transactional
   @Override
   public void initalizer() {
//      //1
//      activeCreditBO.add(00001L, 1L, LocalDateTime.of(2016, 12, 30, 12, 00));
//      activeCreditBO.add(00002L, 1L, LocalDateTime.of(2017, 10, 30, 12, 00));
//      activeCreditBO.add(00003L, 1L, LocalDateTime.of(2017, 4, 30, 12, 00));
//      
//      //2
//      activeCreditBO.add(00004L, 2L, LocalDateTime.of(2018, 1, 30, 12, 00));
//      activeCreditBO.add(00005L, 2L, LocalDateTime.of(2017, 3, 30, 12, 00));
//      activeCreditBO.add(00006L, 2L, LocalDateTime.of(2017, 8, 30, 12, 00));
//      
//      //3
//      activeCreditBO.add(00007L, 3L, LocalDateTime.of(2018, 12, 30, 12, 00));
//      activeCreditBO.add(00010L, 3L, LocalDateTime.of(2017, 5, 30, 12, 00));
//      activeCreditBO.add(00011L, 3L, LocalDateTime.of(2019, 1, 30, 12, 00));
      
   }

   
}
