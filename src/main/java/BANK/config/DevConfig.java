package BANK.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import BANK.sharedkernel.constant.Profiles;

/**
 * This configuration file is responsible for loading external configuration. It is especially useful in overriding
 * default properties by production one. Also it is skipped in TEST profile so it could be run on Continuous Integration
 * without external file.
 *
 * 
 */
@Configuration
@PropertySource("classpath:/defaultConfig.properties")
@Profile({Profiles.DEVELOPMENT})
public class DevConfig {

}
