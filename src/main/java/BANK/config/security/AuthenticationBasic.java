package BANK.config.security;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * This class should be used only in development. It allows to avoid LDAP authentication in favor of quick in memory
 * authentication.
 *
 *
 */
public class AuthenticationBasic
   extends WebSecurityConfigurerAdapter {

 private static final String PASSWORD = "password";
   private static final String ROLE_ADMIN = "ADMIN";

   @Value("${spring.datasource.driverClassName}")
   private String databaseDriverClassName;

   @Value("${spring.datasource.url}")
   private String datasourceUrl;

   @Value("${spring.datasource.username}")
   private String databaseUsername;

   @Value("${spring.datasource.password}")
   private String databasePassword;

   @Autowired
   private DataSource datasource;
   
   @Autowired
   private CustomLogoutHandler customLogoutHandler;
   
   @Autowired
   private CustomAuthenticationHandler successHandler;
   
   @Autowired
   private CustomAuthenticationProvider authenticationProvider;

   @Override
   public void configure(WebSecurity web)
      throws Exception {
      AuthenticationCommonUtil.configure(web);
   }

   @Override
   protected void configure(AuthenticationManagerBuilder auth)
      throws Exception {
//      InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> authManagerConfigurer
//         = auth.inMemoryAuthentication();
//
//      addUsers(authManagerConfigurer);
//      addAdmin(authManagerConfigurer);
      auth.eraseCredentials(false);
      auth.authenticationProvider(authenticationProvider);
//      auth.jdbcAuthentication()
//         .dataSource(datasource)
//         .usersByUsernameQuery(
//            "select username,password, enabled from user where username=?")
//         .authoritiesByUsernameQuery(
//            "select username, role from user where username=?");
   }

   private void addUsers(InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> authManagerConfigurer) {
      authManagerConfigurer
         .withUser("mateusz.ledzewicz").password(PASSWORD).roles();
      authManagerConfigurer
         .withUser("mariusz.klysinski").password(PASSWORD).roles();
   }

   private void addAdmin(InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> authManagerConfigurer) {
      authManagerConfigurer
         .withUser("mateusz.glabicki").password(PASSWORD).roles(ROLE_ADMIN);
   }

   @Override
   protected void configure(HttpSecurity http)
      throws Exception {
      AuthenticationCommonUtil.configure(http,customLogoutHandler,successHandler);
   }

   @Bean(name = "dataSource")
   public DriverManagerDataSource dataSource() {
      DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
      driverManagerDataSource.setDriverClassName(databaseDriverClassName);
      driverManagerDataSource.setUrl(datasourceUrl);
      driverManagerDataSource.setUsername(databaseUsername);
      driverManagerDataSource.setPassword(databasePassword);
      return driverManagerDataSource;
   }
}
