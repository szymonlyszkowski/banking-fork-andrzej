(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveCreditEditCtrl', ActiveCreditEditCtrl);

   ActiveCreditEditCtrl.$inject = [
      '$scope',
      '$state',
//      'activeCredit',
      'ActiveCredit',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function ActiveCreditEditCtrl($scope, $state, /*activeCredits,*/ ActiveCredit, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

//      $scope.activeCredit = {
//         activeCreditId :activeCredit.activeCreditId,
//         rateOfInterest : activeCredit.rateOfInterest,
//         interest : activeCredit.interest,
//         insurance : activeCredit.insurance,
//         installment : activeCredit.installment,
//         provision : activeCredit.provision,
//         finishDate : activeCredit.finishDate,
//         startDate : activeCredit.startDate
//      };
      $scope.saveActiveCredit = saveActiveCredit;

      function saveActiveCredit() {
         var activeCredit = new ActiveCredit();
         activeCredit.rateOfInterest = $scope.activeCredit.rateOfInterest;
         activeCredit.interest = $scope.activeCredit.interest;
         activeCredit.insurance = $scope.activeCredit.insurance;
         activeCredit.installment = $scope.activeCredit.installment;
         activeCredit.provision = $scope.activeCredit.provision;
         activeCredit.finishDate = $scope.activeCredit.finishDate;
         activeCredit.startDate = $scope.activeCredit.startDate;
         activeCredit.activeCreditId = $scope.activeCredit.activeCreditId;
         activeCredit.$update()
                 .then(function (result) {
                    popupService.success('activeCredit-edit.popup.save.success.title', 'activeCredit-edit.popup.save.success.body');
                    $state.go('activeCredit.list');
                 }, function (reason) {
                    popupService.error('activeCredit-edit.popup.save.failure.title', 'activeCredit-edit.popup.save.failure.body');
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();