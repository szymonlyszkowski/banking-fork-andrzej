(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveCreditDetailsCtrl', ActiveCreditDetailsCtrl);

   ActiveCreditDetailsCtrl.$inject = ['$scope', 'activeCredit', 'currencys', 'dateResolveUtil', 'credit',
      'installments', 'Installment', 'popupService'];

   function ActiveCreditDetailsCtrl($scope, activeCredit, currencys, dateResolveUtil, credit,
           installments, Installment, popupService) {

      function _findCurrency(credit) {
         for (var i = 0; i < currencys.length; i++) {
            if (currencys[i].currencyId === credit.currencyId) {
               return currencys[i].symbol;
            }
         }
      }
      credit.currency = _findCurrency(credit);

      $scope.onlyDate = dateResolveUtil.onlyDate;
      $scope.activeCredit = activeCredit;
      $scope.installments = installments;
      $scope.credit = credit;
      $scope.$parent.changeView('DETAILS');

      $scope.payInstallment = _payInstallment;

      function _payInstallment(installmentId) {
         $scope.loading = true;
         Installment.pay({id: installmentId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('activeCredit-details.popup.pay.success.title', 'activeCredit-details.popup.pay.success.body');
                    for (var i = 0; i < $scope.installments.length; i++) {
                       if ($scope.installments[i].installmentId === installmentId) {
                          $scope.installments[i].active = false;
                          $scope.installments[i].paiedAt = new Date().toISOString();
                       }
                    }
                 }
                 , function (reason) {
                    popupService.error( 'activeCredit-details.popup.pay.failure.title','activeCredit-details.popup.pay.failure.body');
                 }        
                 
                 
                 
                 );
      }
   }
})();
