(function () {
   'use strict';

   angular
           .module('bank')
           .config(activeCreditProvider);

   activeCreditProvider.$inject = ['$stateProvider'];

   function activeCreditProvider($stateProvider) {
      var resolveActiveCredits = ['$state', 'ActiveCredit', 'logToServerUtil', loadActiveCredits];
      var resolveInstallments = ['$state', '$stateParams', 'Installment', 'logToServerUtil', loadInstallments];
      var resolveCredits = ['$state', 'Credit', 'logToServerUtil', loadCredits];
      var resolveSingleActiveCredit = ['$state', '$stateParams', 'ActiveCredit', 'logToServerUtil',
         loadSingleActiveCredit];
      var resolveSingleCredit = ['$state', '$stateParams', 'Credit', 'logToServerUtil',
         loadSingleCredit];
      var resolveCurrencys = ['$state', 'Currency', 'logToServerUtil', loadCurrencys];

      $stateProvider
              .state('activeCredit', {
                 parent: 'root',
                 url: '/activeCredit',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('activeCredit.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/activecredit/list',
                 controller: 'ActiveCreditListCtrl',
                 resolve: {
                    activeCredits: resolveActiveCredits,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeCredit.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/activecredit/add',
                 controller: 'ActiveCreditAddCtrl',
                 resolve: {
                    credits: resolveCredits,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeCredit.userAdd', {
                 url: '/add/{userId:int}',
                 reloadOnSearch: false,
                 templateUrl: '/activecredit/add',
                 controller: 'ActiveCreditAddCtrl',
                 resolve: {
                    credits: resolveCredits,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeCredit.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/activecredit/edit',
                 controller: 'ActiveCreditEditCtrl',
                 resolve: {
                    activeCredit: resolveSingleActiveCredit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeCredit.common', {
                 url: '/{id:int}',
                 templateUrl: '/activecredit/common',
                 controller: 'ActiveCreditCommonCtrl',
                 redirectTo: 'activeCredit.common.details',
                 resolve: {
                    activeCredit: resolveSingleActiveCredit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeCredit.common.details', {
                 url: '/details/{creditId:int}',
                 templateUrl: '/activecredit/details',
                 controller: 'ActiveCreditDetailsCtrl',
                 resolve: {
                    currencys: resolveCurrencys,
                    credit: resolveSingleCredit,
                    installments: resolveInstallments,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeCredit/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadActiveCredits($state, ActiveCredit, logToServerUtil) {
         var activeCreditsPromise = ActiveCredit.query().$promise;
         activeCreditsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get ActiveCredits failed', reason);
//                    $state.go('dashboard');
                 });
         return activeCreditsPromise;
      }

      function loadCurrencys($state, Currency, logToServerUtil) {
         var currencyPromise = Currency.query().$promise;
         currencyPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Currency failed', reason);
//                    $state.go('dashboard');
                 });
         return currencyPromise;
      }

      function loadInstallments($state, $stateParams, Installment, logToServerUtil) {
         var activeCreditsPromise = Installment.creditInstallment({id: $stateParams.id}).$promise;
         activeCreditsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Installments failed', reason);
//                    $state.go('dashboard');
                 });
         return activeCreditsPromise;
      }

      function loadCredits($state, Credit, logToServerUtil) {
         var creditsPromise = Credit.query().$promise;
         creditsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get ActiveCredits failed', reason);
                 });
         return creditsPromise;
      }

      function loadSingleActiveCredit($state, $stateParams, ActiveCredit, logToServerUtil) {
         var activeCreditPromise = ActiveCredit.get({id: $stateParams.id}).$promise;
         activeCreditPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get ActiveCredit failed', reason);
//                    $state.go('dashboard');
                 });
         return activeCreditPromise;
      }

      function loadSingleCredit($state, $stateParams, Credit, logToServerUtil) {
         var creditPromise = Credit.get({id: $stateParams.creditId}).$promise;
         creditPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Credit failed', reason);
//                    $state.go('dashboard');
                 });
         return creditPromise;
      }
   }
})();
