(function () {
   'use strict';

   angular
           .module('bank')
           .factory('User', User);

   User.$inject = ['$resource', 'dateResolveUtil'];

   function User($resource, dateResolveUtil) {
      return $resource('/api/user/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/user/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         block : {
            method:'DELETE',
            url: '/api/user/block/:id',
            params:{id : '@id'}
         },
         unblock: {
            method:'PUT',
            url: '/api/user/unlock/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/user/delete/:id',
            params:{id : '@id'}
         },
         titles: {
            method: 'GET',
            url: '/api/user/title',
            isArray: true
         },
         getLoggedUser: {
            method: 'GET',
            params: {},
            url: '/api/user/logged',
            transformResponse: transformResponse
         },
         active: {method: 'GET',
            url: '/api/user/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(user) {
//         user.createdAt = dateResolveUtil.convertToDate(user.createdAt);
//         user.updatedAt = dateResolveUtil.convertToDate(user.updatedAt);
//         user.blockedAt = dateResolveUtil.convertToDate(user.blockedAt);
      }
   }
})();
