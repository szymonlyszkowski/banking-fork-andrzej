(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserDetailsCtrl', UserDetailsCtrl);

   UserDetailsCtrl.$inject = ['$scope', '$state', 'ActiveCredit', 'Myaccount', 'dateResolveUtil',
      'user', 'activeCredits', 'accounts', 'currencys','popupService'];

   function UserDetailsCtrl($scope, $state, ActiveCredit, Myaccount, dateResolveUtil, user, activeCredits, accounts, currencys,popupService) {
//      $scope.currencys = currencys;
      $scope.user = user;
      $scope.activeCredits = activeCredits;
      $scope.accounts = accounts;
      $scope.onlyDate = dateResolveUtil.onlyDate;
      $scope.finishActiveCredit = finishActiveCredit;
      $scope.deleteMyaccount = deleteMyaccount;
      $scope.boost = boost;
      $scope.changeView = changeView;

      function changeView(state) {
         $scope.view = {
            details: false,
            accounts: false,
            credits: false
         };
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            case 'ACCOUNTS':
               $scope.view.accounts = true;
               break;
            case 'CREDITS':
               $scope.view.credits = true;
               break;
            default:
               break;
         }
      }

      for (var i = 0; i < $scope.accounts.length; i++) {
         $scope.accounts[i].currency = _findCurrency($scope.accounts[i]);
      }

      function _findCurrency(account) {
         for (var i = 0; i < currencys.length; i++) {
            if (currencys[i].currencyId === account.currencyId) {
               return currencys[i].symbol;
            }
         }
      }

//      $scope.deleteActiveCredit = deleteActiveCredit;


//      function deleteActiveCredit(activeCreditId) {
//         $scope.loading = true;
//         ActiveCredit.delete({id: activeCreditId}).$promise
//                 .then(function (data) {
//                    $scope.loading = false;
//                    for (var i = 0; i < $scope.activeCredits.length; i++) {
//                       if ($scope.activeCredits[i].activeCreditId === activeCreditId) {
//                          $scope.activeCredits.splice(i, 1);
//                       }
//                    }
//                 });
//      }

      function finishActiveCredit(activeCreditId) {
         $scope.loading = true;
         ActiveCredit.finish({id: activeCreditId}).$promise
                 .then(function (data) {
                    popupService.success('user-details.popup.finish.success.title', 'user-details.popup.finish.success.body');
                    for (var i = 0; i < $scope.activeCredits.length; i++) {
                       if ($scope.activeCredits[i].activeCreditId === activeCreditId) {
                          $scope.activeCredits[i].active = false;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function deleteMyaccount(myaccountId) {
         $scope.loading = true;
         Myaccount.delete({id: myaccountId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('user-details.popup.delete.success.title', 'user-details.popup.delete.success.body');
                    for (var i = 0; i < $scope.accounts.length; i++) {
                       if ($scope.accounts[i].accountId === myaccountId) {
                          $scope.accounts.splice(i, 1);
                       }
                    }
                 });
      }

      function boost(accountId) {
         $state.go('myaccount.boost', {id: accountId, userId: $scope.user.userId})
      }

      $scope.changeView('DETAILS');
   }
})();
