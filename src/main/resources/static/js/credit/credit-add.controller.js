(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditAddCtrl', CreditAddCtrl);

   CreditAddCtrl.$inject = [
      '$scope',
      '$state',
      'currencys',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'languageUtil',
      'popupService'];  

   function CreditAddCtrl($scope, $state, currencys, Credit, CommonUtilService, logToServerUtil, dateResolveUtil, languageUtil,popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.currencys = currencys;

      $scope.credit = {
         provision: '',
         interest: '',
         insurance: false,
         installment: '',
         changingRate: false,
         contribution: '',
         maxValue: '',
         minValue: '',
         type: ''
      };

      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.changingRate = $scope.credit.changingRate;
         credit.interest = $scope.credit.interest;
         credit.insurance = $scope.credit.insurance;
         credit.installment = $scope.credit.installment;
         credit.provision = $scope.credit.provision;
         credit.contribution = $scope.credit.contribution;
         credit.minValue = $scope.credit.minValue;
         credit.maxValue = $scope.credit.maxValue;
         credit.type = $scope.credit.type;
         credit.currencyId = $scope.credit.currencyId


         credit.$save()
                 .then(function (result) {
                    $state.go('credit.list');
                    popupService.success('credit-add.popup.save.success.title', 'credit-add.popup.save.success.body');
                 }, function (reason) {
                    logToServerUtil.trace('Save Credit failure', reason);
                    popupService.error('credit-add.popup.save.failure.title', 'credit-add.popup.save.failure.body');
                 });
      }
   }
})();