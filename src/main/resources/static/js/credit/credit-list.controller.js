(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditListCtrl', CreditListCtrl);

   CreditListCtrl.$inject = [
      '$scope',
      'Credit',
      'credits',
      '$stateParams',
      '$location',
      'popupService'];

   function CreditListCtrl($scope, Credit, credits, $stateParams, $location, popupService) {
      $scope.loading = false;
      $scope.credits = credits;

      $scope.refresh = refresh;
      $scope.finishCredit = finishCredit;
      $scope.deleteCredit = deleteCredit;
      $scope.radioModel = 0;


      function reloadCredits() {
         $scope.loading = true;
         Credit.query().$promise
                 .then(function (data) {
                    $scope.credits = data;
                    $scope.loading = false;
                 });
      }

      function deleteCredit(creditId) {
         $scope.loading = true;
         Credit.delete({id: creditId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('credit-list.popup.save.success.title', 'credit-list.popup.save.success.body');
                    for (var i = 0; i < $scope.credits.length; i++) {
                       if ($scope.credits[i].creditId === creditId) {
                          $scope.credits.splice(i, 1);
                       }
                    }
                 });
      }

      function finishCredit(creditId) {
         $scope.loading = true;
         Credit.finish({id: creditId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.credits.length; i++) {
                       if ($scope.credits[i].creditId === creditId) {
                          $scope.credits[i].active = false;
                       }
                    }
                    popupService.success('credit-list.popup.save.success.title', 'credit-list.popup.save.success.body');
                    $scope.loading = false;
                 });
      }

      function refresh() {
         Credit.refresh().$promise
                 .then(function () {
                    reloadCredits();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();