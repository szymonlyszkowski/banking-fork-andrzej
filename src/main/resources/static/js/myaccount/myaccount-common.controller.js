(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountCommonCtrl', MyaccountCommonCtrl);

   MyaccountCommonCtrl.$inject = ['$scope'/*, 'myaccount'*/];

   function MyaccountCommonCtrl($scope /*, myaccount*/) {
//      $scope.myaccount = myaccount;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
