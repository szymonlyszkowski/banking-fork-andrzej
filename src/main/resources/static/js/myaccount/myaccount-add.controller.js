(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountAddCtrl', MyaccountAddCtrl);

   MyaccountAddCtrl.$inject = [
      '$scope',
      '$state',
      'currencys',
      'Myaccount',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'popupService'];

   function MyaccountAddCtrl($scope, $state, currencys, Myaccount, CommonUtilService, logToServerUtil, dateResolveUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.currencys = currencys;

      $scope.myaccount = {
         iban: '',
         balance: '',
         currencyId: ''
      };

      $scope.saveMyaccount = saveMyaccount;
      $scope.cancel = cancel;

      function cancel() {
         $state.go('user.common', {id: $state.params.userId});
      }

      function saveMyaccount() {
         var myaccount = new Myaccount();
         myaccount.iban = $scope.myaccount.iban;
         myaccount.balance = $scope.myaccount.balance;
         myaccount.currencyId = $scope.myaccount.currencyId;
         myaccount.userId = $state.params.userId;

         myaccount.$save()
                 .then(function (result) {
//                    $state.go('myaccount.list');
                    popupService.success('myaccount-add.popup.save.success.title', 'myaccount-add.popup.save.success.body');
                    $state.go('user.common', {id: $state.params.userId});
                 }, function (reason) {
                    popupService.error('myaccount-add.popup.save.failure.title', 'myaccount-add.popup.save.failure.body');
                    logToServerUtil.trace('Save Myaccount failure', reason);
                 });
      }
   }
})();